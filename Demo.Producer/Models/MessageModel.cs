namespace DemoRabbitServices.Models
{
    public class MessageModel
    {
        public MessageModel()
        {
            Id = Guid.NewGuid();
        }

        public Guid Id { get; set; }
        public string Content { get; set; }
    }
}


