using Demo.Core.Mappers;
using DemoRabbitServices.Constants;
using RabbitMQ.Client.Core.DependencyInjection;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
// Auto Mapper
builder.Services.AddAutoMapper(typeof(MapperProfile));
// Rabbit MQ Producer
var rabbitMqProducerSection = builder.Configuration.GetSection("RabbitMqProducer");
var producingExchangeSection = builder.Configuration.GetSection("ProducingExchange");
builder.Services
    .AddRabbitMqProducer(rabbitMqProducerSection)
    .AddProductionExchange(DemoConstants.DefaultRMQExchange, producingExchangeSection);

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseAuthorization();

app.MapControllers();

app.Run();
