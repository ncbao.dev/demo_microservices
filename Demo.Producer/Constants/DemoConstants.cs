namespace DemoRabbitServices.Constants {
    public static class DemoConstants {
        public static readonly string DefaultRMQExchange = "demo.exchange";
        public static readonly string DefaultRMQQueue = "demo.queue";
        public static readonly string DefaultRMQKey = "demo.key";
    }
}