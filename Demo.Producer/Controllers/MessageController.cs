using Demo.Core.Contracts;
using DemoRabbitServices.Constants;
using Microsoft.AspNetCore.Mvc;
using RabbitMQ.Client.Core.DependencyInjection.Services.Interfaces;

namespace DemoRabbitServices.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class MessageController : ControllerBase
    {
        private IProducingService _producingService;
        private ILogger<MessageController> _logger;

        public MessageController(IProducingService producingService, ILogger<MessageController> logger)
        {
            _producingService = producingService;
            _logger = logger;
        }

        [HttpPost]
        public async Task<IActionResult> Create([FromBody] RequestCreateMessage request)
        {
            try {
                await _producingService.SendAsync<RequestCreateMessage>(request, DemoConstants.DefaultRMQExchange, DemoConstants.DefaultRMQKey);
                _logger.LogInformation("POST: /message - Success");
            return Ok();
            } catch (Exception ex) {
                _logger.LogError(ex.Message.ToString());
                return StatusCode(500);
            }
        }
    }
}