﻿using DemoRabbitServices.RabbitMQ.Interfaces;
using RabbitMQ.Client;

namespace DemoRabbitServices.RabbitMQ.Implements
{
    public class RabbitMqProducer : IRabbitMqProducer, IDisposable
    {
        private IConnection rabbitConn;
        private IModel rabbitChannel;
       
        public RabbitMqProducer(ConnectionFactory factory)
        {
            rabbitConn = factory.CreateConnection();
            rabbitChannel = rabbitConn.CreateModel();
        }

        public async Task SendMessage()
        {
            throw new NotImplementedException();
        }

        public void Dispose()
        {
            rabbitChannel.Dispose();
            rabbitConn.Dispose();
        }
    }
}
