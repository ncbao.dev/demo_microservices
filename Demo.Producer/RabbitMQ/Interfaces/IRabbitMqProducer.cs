﻿namespace DemoRabbitServices.RabbitMQ.Interfaces
{
    interface IRabbitMqProducer
    {
        Task SendMessage();
    }
}
