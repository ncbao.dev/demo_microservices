using AutoMapper;
using Demo.Core.Contracts;
using Demo.Core.Entities;

namespace Demo.Core.Mappers {
    public class MapperProfile : Profile
    {
        public MapperProfile()
        {
            CreateMap<RequestCreateMessage, Message>().ReverseMap();
        }
    }
}