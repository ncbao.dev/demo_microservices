namespace Demo.Core.Contracts {
    public class RequestCreateMessage
    {
        public string Content { get; set; }
    }
}