using System.Text;
using System.Text.Json;
using AutoMapper;
using Demo.Consumer.Data;
using Demo.Core.Contracts;
using Demo.Core.Entities;
using Microsoft.Extensions.Logging;
using RabbitMQ.Client.Core.DependencyInjection.MessageHandlers;
using RabbitMQ.Client.Core.DependencyInjection.Models;

namespace RabbitDBConsumer {
    public class MessageHandler : IAsyncMessageHandler
    {
        private ILogger<MessageHandler> _logger;
        private IMapper _mapper;
        private AppDbContext _context;

        public MessageHandler(ILogger<MessageHandler> logger, IMapper mapper, AppDbContext context)
        {
            _logger = logger;
            _mapper = mapper;
            _context = context;
        }

        async Task IAsyncMessageHandler.Handle(MessageHandlingContext context, string matchingRoute)
        {
            var serializeBody = Encoding.UTF8.GetString(context.Message.Body.ToArray());
            var message = JsonSerializer.Deserialize<RequestCreateMessage>(serializeBody);

            _logger.LogInformation($"Received: {serializeBody}");
            context.AcknowledgeMessage();

            await SaveIntoDB(message!);
        }

        async Task SaveIntoDB(RequestCreateMessage request) {
            var messageEntity = _mapper.Map<Message>(request);
            
            await _context.Messages.AddAsync(messageEntity);
            await _context.SaveChangesAsync();
        }
    }
}