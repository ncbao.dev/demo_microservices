﻿
using Demo.Consumer.Data;
using Demo.Core.Mappers;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using RabbitDBConsumer;
using RabbitDBConsumer.Constants;
using RabbitMQ.Client.Core.DependencyInjection;

public class Program
{
    public static async Task Main(string[] args)
    {
        var host = new HostBuilder()
                            .ConfigureAppConfiguration((hostingContext, config) => {
                                config.AddJsonFile("appsettings.json", optional: false, reloadOnChange: true);
                            })
                            .ConfigureLogging((hostingContext, logging) => {
                                logging.AddConfiguration(hostingContext.Configuration.GetSection("Logging"));
                                logging.AddConsole();
                            })
                            .ConfigureServices((hostingContext, services) => {
                                var rabbitMqConsumerSection = hostingContext.Configuration.GetSection("RabbitMqConsumer");
                                var consumingExchangeSection = hostingContext.Configuration.GetSection("ConsumingExchange");
                                var dbConnectionString = hostingContext.Configuration.GetConnectionString("Demo.MSSQL");

                                services.AddAutoMapper(typeof(MapperProfile));

                                services.AddDbContext<AppDbContext>((options) => {
                                    options.UseSqlServer(dbConnectionString);
                                    options.UseLoggerFactory(LoggerFactory.Create((builder) => {
                                        builder.AddConsole();
                                    }));
                                    options.EnableSensitiveDataLogging();
                                });

                                services.AddRabbitMqServices(rabbitMqConsumerSection)
                                        .AddConsumptionExchange(DemoConstants.DefaultRMQExchange, consumingExchangeSection)
                                        .AddAsyncMessageHandlerSingleton<MessageHandler>(DemoConstants.DefaultRMQKey);

                            });
        
        await host.RunConsoleAsync();
    }
}