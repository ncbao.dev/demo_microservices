using Demo.Core.Entities;
using Microsoft.EntityFrameworkCore;

namespace Demo.Consumer.Data
{
    public class AppDbContext : DbContext
    {
        public AppDbContext(DbContextOptions options) : base(options)
        {
        }

        public virtual DbSet<Message> Messages { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            base.OnConfiguring(optionsBuilder);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Message>((entity) => {
                entity.HasKey(e => e.Id);

                entity.Property(e => e.Id)
                        .ValueGeneratedOnAdd();

                entity.Property(e => e.Content)
                        .IsRequired()
                        .HasMaxLength(1000);
            });

            base.OnModelCreating(modelBuilder);
        }
    }
}